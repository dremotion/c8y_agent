
/*
1. Delete device from cloud -> Re-register
2. Improper modifcation of response file "haha.txt"
3. Registeration flag should be saved in nvm (not possible so go with file)
4. HTTP Response Code Check to be added
*/


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>


#define DEBUG_ENABLE	1

#define SET		1
#define CLEAR		0
#define FUNC_SUCCESS	1
#define FUNC_FAIL	0


#define ASSIGN_EXTERNAL_ID  "Room204_Sensor"
#define TIME_ZONE	    +05:30



char response[10000] = {"0"};
char *dev_id_ptr, dev_id[20] = {"0"};
char *ex_id_ptr, ex_id[20] = {"0"};


/*
 * Function: Read_File_To_Buffer()
 * Purpose : Reads File upto the given argument string
 * Params  : Takes the string to be searched
 */
void  Read_File_To_Buffer(char *str2search)
{
        FILE *response_fp;
        char ch;
        int i =0;
        response_fp = fopen("haha.txt","r");
//If File exists
        if(response_fp == 0)
        {
                return;
        }

//Copy file to buffer
        while(((ch = fgetc(response_fp)) != EOF) || (i>=9998))
        {
                response[i++] = (char)ch;
                if((strstr(response, str2search)) != 0)
                {
                        break;
                }
        }
        fclose(response_fp);
        response[i] = '\0';
}



/*
 * Function: Read_Dev_Id()
 * Purpose : Reads device Id assigned by cumulocity
 * Params  : Takes the string to be searched
 * Returns : 1 on success
 *	     0 on failure
 */
unsigned long int Read_Dev_Id(char *str2search)
{
	int id = 0;
//Check for string in buffer
	if((dev_id_ptr = (strstr(response, str2search))) != 0)
	{
		while(*dev_id_ptr++ != '/');
		while(*dev_id_ptr != '\r')
		{
			dev_id[id++] = *dev_id_ptr++;
		}
#ifdef DEBUG_ENABLE
		printf("Device ID = %s\n",dev_id);
#endif
		return FUNC_SUCCESS;
	}
	return FUNC_FAIL;
}





/*
 * Function: Read_External_Id()
 * Purpose : Reads external id
 * Returns : 1 if extenal id found
 *           0 on failure
 */
unsigned short int Read_External_ID(char *str2search)
{
        int id = 0;
//Check for string in buffer
        if((ex_id_ptr = (strstr(response, str2search))) != 0)
        {
                while(*ex_id_ptr++ != '[');
                while(*ex_id_ptr != ']')
                {
                        ex_id[id++] = *ex_id_ptr++;
                }
#ifdef DEBUG_ENABLE
                printf("External ID = %s\n",ex_id);
#endif
                return FUNC_SUCCESS;
        }
        return FUNC_FAIL;

}




/*
 * Function : Extract_Date_Time 
 * Purpose  : Saves current date and time in given buffer
 * Progress : Under construction - Do not Use
 */
void Extract_Date_Time()
{
        FILE *response_fp;
        char ch;
        int i = 0;
        response_fp = fopen("haha.txt","r");
//If File exists
        if(response_fp == 0)
        {
                return;
        }

//Copy file to buffer
        while(((ch = fgetc(response_fp)) != EOF) || (i>=9998))
        {
                response[i++] = (char)ch;
        }
        fclose(response_fp);
        response[i] = '\0';

//data is in response buffer
}



/****************************** MAIN ***************************************/
int main()
{
	int v = 0;
	char command[2048] = {"0"};
	unsigned short int device_register_complete_flag = CLEAR;
	if(device_register_complete_flag != SET)
	{
		/*POST: Create Device*/
		if(fork() == 0)
		{
			system("curl -is -H \"Content-Type: application/json\" -d '{\"name\": \"testMeasurementDevice\", \"c8y_IsDevice\": {}, \"c8y_SupportedMeasurements\": [\"c8y_TemperatureMeasurement\"]}' --user sendme@sharklasers.com:mainhoon http://camel.cumulocity.com/inventory/managedObjects > haha.txt");
			exit(0);
		}
		wait(0); //Hold until child exits to prevent zombie
#ifdef DEBUG_ENABLE
		printf("Child 1 exit: Device Creation Finished\n");
#endif

		Read_File_To_Buffer("Strict-Transport");
		Read_Dev_Id("managedObjects");//extracts device id
		/*GET: check if any external ID*/
		if(fork() == 0)
		{
                        strcpy(command, "curl -is --user sendme@sharklasers.com:mainhoon http://camel.cumulocity.com/identity/globalIds/");
			strcat(command, dev_id);
			strcat(command, "/externalIds > haha.txt");
			system(command);
			exit(0);
		}
		wait(0);
#ifdef DEBUG_ENABLE
		printf("Child 2 exit: Check for external ID\n");
#endif
		Read_File_To_Buffer("ids/globalIds/");
		if((Read_External_ID("externalIds\":")) != 0)//If no external id present
		{
			strcpy(command, "curl -is -H \"Content-Type: application/json\" -d '{\"externalId\": \"ASSIGN_EXTERNAL_ID\",\"type\": \"testExternalId\"}'");
                        strcat(command, " --user sendme@sharklasers.com:mainhoon http://camel.cumulocity.com/identity/globalIds/");
                        strcat(command, dev_id);
                        strcat(command, "/externalIds > haha.txt");
                        system(command);
		}
	device_register_complete_flag = SET;
	}

	int time_wastage = 0;
	while(1)
	{
	//send data from sensor
	//sleep
		time_wastage = 0;

		if(fork == 0)
		{
			strcpy(command, "date > haha.txt");
			system(command);
			exit(0);
		}
		wait(0);


		if(fork() == 0)
		{
//			strcpy(command, "curl -is -H \"Content-Type: application/json\" -d '{\"c8y_TemperatureMeasurement\": {\"T\": {\"value\": 9999, \"unit\": \"C\" } }, \"time\":\"2018-06-13T13:05:27.000+05:30\", \"source\": {\"id\":\"1208\" }, \"type\": \"c8y_TemperatureMeasurement\"}' --user sendme@sharklasers.com:mainhoon http://camel.cumulocity.com/measurement/measurements > haha.txt");
			strcpy(command, "curl -v -is -H \"Content-Type: application/json\" -d '{\"c8y_TemperatureMeasurement\": {\"T\": {\"value\": 1111, \"unit\": \"C\" } }, \"time\":\"2018-06-13T13:14:14.000+05:30\", \"source\": {\"id\":\"");
			strcat(command, dev_id);
			strcat(command, "\" }, \"type\": \"c8y_TemperatureMeasurement\"}' --user sendme@sharklasers.com:mainhoon http://camel.cumulocity.com/measurement/measurements");
			printf("%s\n",command);
			system(command);
			exit(0);
		}
		wait(0);
		while(time_wastage != 60000)
		{
			time_wastage++;
		}
	}
	return 0;
}
